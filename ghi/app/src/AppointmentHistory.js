import {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentHistory() {
    const navigate = useNavigate();
    const [data, setData] = useState(null)
    const [vin, setVin] = useState('');
    const [filterStr, setFilterStr] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8080/api/appointments/history/';
                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const responseData = await response.json();
                setData(responseData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, [setData]);

    function handleFilter(filterStr) {
        setFilterStr(filterStr);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    return (
        <div className="container">
            <button
                className="btn btn-danger"
                onClick={() => navigate(`/appointments/new/`)}
                >Add New Appointment
            </button>

            {data && data.appointments ? (
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <h1>Service History</h1>
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <input className="form-control" value={vin} onChange={handleVinChange} placeholder="Search by VIN..." required type="text" name="vin" id="vin" />
                                </div>
                                <div className="col">
                                    <button
                                        className="btn btn-success"
                                        onClick={() => handleFilter(document.getElementById('vin').value)}
                                        >Search
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table className="table table-striped text-nowrap">
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th>Is VIP?</th>
                                    <th>Customer</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Technician</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.appointments
                                    .filter((appointment) => appointment.vin.toLowerCase().includes(filterStr.toLowerCase()))
                                    .map((appointment, index) => (
                                        <tr key={appointment.id || index}>
                                            <td>{appointment.vin}</td>
                                            <td>{appointment.is_vip ? "Yes" : "No"}</td>
                                            <td>{appointment.customer}</td>
                                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                                            <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.status[0].toUpperCase() + appointment.status.substring(1)}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default AppointmentHistory;
