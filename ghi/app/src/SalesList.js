import {useState, useEffect} from 'react';

function SaleList() {
    const [sales, setSales] = useState([]);

    const getData = async () => {
        const request = await fetch('http://localhost:8090/api/sales/');
        if (request.ok) {
            const resp = await request.json();
            setSales(resp.sales);
        } else {
            console.error("Request Error");
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="px-5">
        <h1>Sales</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {sales.map(sale => {
                return (
                <tr key={sale.id}>
                    <td>{ sale.salesperson.employee_id }</td>
                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                    <td>{ sale.automobile.vin }</td>
                    <td>{ sale.price.toFixed(2) }</td>
                </tr>
                )
            })}
          </tbody>
        </table>
      </div>
    );
}

export default SaleList;
