import React, { useEffect, useState } from 'react';

function TechnicianForm () {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const fetchTechniciansData = async () => {
        const techniciansUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(techniciansUrl);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchTechniciansData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = new Date(`${date} ${time}`);
        data.technician = technician;
        data.reason = reason;
        data.status = "active";

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTechnician('');
            setReason('');
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input className="form-control" value={vin} onChange={handleVinChange} placeholder="VIN" type="text" id="vin" name="vin" required />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" value={customer} onChange={handleCustomerChange} placeholder="Customer" type="text" id="customer" name="customer" required />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" value={date} onChange={handleDateChange} type="date" id="date" name="date" min="2023-12-19" max="2024-12-31" required />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" value={time} onChange={handleTimeChange} type="time" id="time" name="time" min="09:00" max="18:00" required />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select className="form-select" value={technician} onChange={handleTechnicianChange} name="technician" id="technician" required>
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>
                                            {`${technician.first_name} ${technician.last_name}`}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" value={reason} onChange={handleReasonChange} placeholder="Reason"  type="text" id="reason" name="reason" required />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
