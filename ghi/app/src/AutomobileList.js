import {useState, useEffect} from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    const getData = async () => {
        const request = await fetch('http://localhost:8100/api/automobiles/');
        if (request.ok) {
            const resp = await request.json();
            setAutomobiles(resp.autos);
        } else {
            console.error("Request Error");
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="px-5">
        <h1>Automobiles</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Sold</th>
            </tr>
          </thead>
          <tbody>
            {automobiles.map(automobile => {
                return (
                    <tr key={automobile.id} value={automobile.id}>
                        <td>{automobile.vin}</td>
                        <td>{automobile.color}</td>
                        <td>{automobile.year}</td>
                        <td>{automobile.model.name}</td>
                        <td>{automobile.model.manufacturer.name}</td>
                        <td>{automobile.sold ? (<span> Sold </span>) : ( <span> Not Sold </span>)}</td>
                    </tr>
                )
            })}
          </tbody>
        </table>
      </div>
    );
}

export default AutomobileList;
