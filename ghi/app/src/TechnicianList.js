import {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function TechnicianList() {
    const [data, setData] = useState(null)

    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8080/api/technicians/';
                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const responseData = await response.json();
                setData(responseData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, [setData]);

    return (
        <div className="container">
            <button
                className="btn btn-danger"
                onClick={() => navigate(`/technicians/new/`)}
                >Add New Technician
            </button>

            {data && data.technicians ? (
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.technicians.map((technician, index) => (
                                <tr key={technician.id || index}>
                                    <td>{technician.employee_id}</td>
                                    <td>{technician.first_name}</td>
                                    <td>{technician.last_name}</td>
                                </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default TechnicianList;
