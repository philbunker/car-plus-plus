import React, {useState, useEffect} from 'react';

function SaleForm() {
    const[automobile, setAutomobile] = useState('');
    const[automobiles, setAutomobiles] = useState([]);
    const[selectedAutomobile, setSelectedAutomobile] = useState(null);
    const[salesperson, setSalesperson] = useState('');
    const[salespeople, setSalespeople] = useState([]);
    const[customer, setCustomer] = useState('');
    const[customers, setCustomers] = useState([]);
    const[price, setPrice] = useState('');


    const fetchAutomobileData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        } else {
            console.error(response);
        }
    }

    const loadCustomers = async ()  => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        } else {
            console.error(response);
        }
    }

    const loadSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        } else {
            console.error(response);
        }
    }

    useEffect(() =>{
        fetchAutomobileData();
        loadCustomers();
        loadSalespeople();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();


      const data = {
        automobile: selectedAutomobile.vin,
        salesperson,
        customer,
        price,
      };

      const saleUrl = "http://localhost:8090/api/sales/";
      const autoUrl = `http://localhost:8100/api/automobiles/${selectedAutomobile.vin}/`;
  
      
      try {
        const [autoResponse, response] = await Promise.all([
            fetch(autoUrl, {
                method: "put",
                body: JSON.stringify({ sold: true }),
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
            fetch(saleUrl, {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        ]);

        
        if (response.ok && autoResponse.ok){
            const newSale = await response.json();
            setSelectedAutomobile(null);
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
      } catch (error) {
        console.error(error);
    }
  };


  const handleAutomobileChange = (event) => {
      const value = event.target.value;
      const selectedAuto = automobiles.find(auto => auto.vin === value);
      if (selectedAuto) {
        setSelectedAutomobile(selectedAuto);
        setAutomobile(value);
      } 
  };

  const handleSalespersonChange = (event) => {
      const value = event.target.value;
      setSalesperson(value);
  };

  const handleCustomerChange = (event) => {
      const value = event.target.value;
      setCustomer(value);
  };

  const handlePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value);
  };


  return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an Sale</h1>
          <form onSubmit={handleSubmit} id="add-sale">
            <div className="mb-3">
            <select onChange={handleAutomobileChange} value={automobile} required name="vin" id="vin" className="form-select">
              <option value="">Choose a Vin...</option>
              {automobiles.filter(automobile => automobile.sold === false).map(automobile => {
                return (
                  <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                )
              })}
              </select>
              </div>
              <div className="mb-3">
            <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
              <option value="">Choose a Salesperson...</option>
              {salespeople.map(salesperson => {
                return (
                  <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                )
              })}
              </select>
              </div>
              <div className="mb-3">
            <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
              <option value="">Choose a Customer...</option>
              {customers.map(customer => {
                return (
                  <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                )
              })}
              </select>
              </div>
              <div className="form-floating mb-3">
              <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" min="0" name="price" id="price" className="form-control"/>
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default SaleForm;
