import {useState, useEffect} from 'react';

function SalespeopleList() {
    const [salespeoples, setSalespeoples] = useState([]);

    const getData = async () => {
        const request = await fetch('http://localhost:8090/api/salespeople/');
        if (request.ok) {
            const resp = await request.json();
            setSalespeoples(resp.salespeople);
        } else {
            console.error("Request Error");
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="px-5">
        <h1>Salespeople</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {salespeoples.map(salepeople => {
                return (
                    <tr key={salepeople.id} value={salepeople.id}>
                        <td>{salepeople.employee_id}</td>
                        <td>{salepeople.first_name}</td>
                        <td>{salepeople.last_name}</td>
                    </tr>
                )
            })}
          </tbody>
        </table>
      </div>
    );
}

export default SalespeopleList;
