import {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentList() {
    const [data, setData] = useState(null)
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8080/api/appointments/';
                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const responseData = await response.json();
                setData(responseData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, [setData]);

    const handleStatus = async (appointmentId, status) => {
        try {
            const statusUrl = `http://localhost:8080/api/appointments/${appointmentId}/${status}/`;
            await fetch(statusUrl, { method: 'PUT' });

            const fetchData = async () => {
                try {
                    const url = `http://localhost:8080/api/appointments/`;
                    const response = await fetch(url);
                    if (!response.ok) {
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }
                    const responseData = await response.json();
                    setData(responseData);
                } catch (error) {
                    console.error('Error fetching data:', error);
                }
            };

            fetchData();
        } catch (error) {
            console.error(`Error ${status}ing appointment:`, error);
        }
    };

    return (
        <div className="container">
            <button
                className="btn btn-danger"
                onClick={() => navigate(`/appointments/new/`)}
                >Add New Appointment
            </button>

            {data && data.appointments ? (
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <h1>Service Appointments</h1>
                        <table className="table table-striped text-nowrap">
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th>Is VIP?</th>
                                    <th>Customer</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Technician</th>
                                    <th>Reason</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.appointments.map((appointment, index) => (
                                    <tr key={appointment.id || index}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.is_vip ? "Yes" : "No"}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                        <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                                        <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                                        <td>{appointment.reason}</td>
                                        <td>
                                            <button
                                                className="btn btn-success"
                                                onClick={() => handleStatus(appointment.id, 'cancel')}
                                                >Cancel
                                            </button>
                                        </td>
                                        <td>
                                            <button
                                                className="btn btn-danger"
                                                onClick={() => handleStatus(appointment.id, 'finish')}
                                                >Finish
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default AppointmentList;
