import React, { useState } from 'react';

function SalespeopleForm () {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeID;

    const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      const newSalesperson = await response.json();

      setFirstName('');
      setLastName('');
      setEmployeeID('');
    }
  }

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleEmployeeIDChange = (event) => {
    const value = event.target.value;
    setEmployeeID(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add New Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input value={firstName} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
              <label htmlFor="firstName">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={lastName} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
              <label htmlFor="lastName">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employeeID} onChange={handleEmployeeIDChange} placeholder="Employee ID" required type="text" name="employeeID" id="employeeID" className="form-control" />
              <label htmlFor="employeeID">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalespeopleForm;
