import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

function ManufacturerList() {
    const [data, setData] = useState(null)

    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = `http://localhost:8100/api/manufacturers/`;
                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const responseData = await response.json();
                setData(responseData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, [setData]);

    return (
        <div className="container">
            <button
                className="btn btn-danger"
                onClick={() => navigate(`/manufacturers/new/`)}
            >Add New Manufacturer
            </button>

            {data && data.manufacturers ? (
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.manufacturers.map((manufacturer, index) => (
                                    <tr key={manufacturer.id || index}>
                                        <td>{manufacturer.name}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default ManufacturerList;
