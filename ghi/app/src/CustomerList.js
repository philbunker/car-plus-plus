import {useState, useEffect} from 'react';

function CustomerList() {
    const [customers, setCustomers] = useState([]);

    const getData = async () => {
        const request = await fetch('http://localhost:8090/api/customers/');
        if (request.ok) {
            const resp = await request.json();
            setCustomers(resp.customers);
        } else {
            console.error("Request Error");
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="px-5">
        <h1>Customers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Phone Number</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {customers.map(customer => {
                return (
                    <tr key={customer.id} value={customer.id}>
                        <td>{customer.first_name}</td>
                        <td>{customer.last_name}</td>
                        <td>{customer.phone_number}</td>
                        <td>{customer.address}</td>
                    </tr>
                )
            })}
          </tbody>
        </table>
      </div>
    );
}

export default CustomerList;
