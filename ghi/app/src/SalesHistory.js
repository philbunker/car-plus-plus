import {useState, useEffect} from 'react';

function SalesHistory() {
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);


    const getSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };

    const getSales = async () => {
        const request = await fetch('http://localhost:8090/api/sales/');
        if (request.ok) {
            const resp = await request.json();
            setSales(resp.sales);
        } else {
            console.error("Request Error");
        }
    };

    useEffect(() => {
        getSales();
        getSalespeople();
    }, []);

    const handleSelectChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    return (
        <div className="container">
            <div className = "container mb-3">
                <h1 className="mt-4">Sales History</h1>
                <select onChange={handleSelectChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a salesperson...</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        )
                    })}
                </select>
            </div>
            <div className="container mb-3">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.filter(sale => sale.salesperson.id === parseInt(salesperson)).map(sale => {
                            return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}       

export default SalesHistory;
