import {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function ModelList() {
    const [data, setData] = useState(null)

    const navigate = useNavigate();

    useEffect(() => {
      const fetchData = async () => {
          try {
            const url = `http://localhost:8100/api/models/`;
            const response = await fetch(url);
            if (!response.ok) {
              throw new Error(`HTTP error! Status: ${response.status}`);
            }
            const responseData = await response.json();
            setData(responseData);
          } catch (error) {
            console.error('Error fetching data:', error);
          }
      };

      fetchData();
    }, [setData]);

    return (
        <div className="container">
            <button
                className="btn btn-danger"
                onClick={() => navigate(`/models/new/`)}
                >Add New Model
            </button>

            {data && data.models ? (
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th>Name</th>
                                <th>Picture</th>
                                <th>Manufacturer</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.models.map((model, index) => (
                                <tr key={model.id || index}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img src={model.picture_url} className="img-fluid thumbnail" alt="model thumbnail" /></td>
                                </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default ModelList;
