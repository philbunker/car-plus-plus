***
# CarCar :blue_car: :red_car: :racing_car: :taxi: :articulated_lorry: :bus:

Team:

* Robert Anderson - Sales
* Phil Bunker - Services

***

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository
2. Clone the forked repository onto your local computer:

```
git clone https://gitlab.com/philbunker/project-beta.git
```

3. Build and run the project using Docker with these commands:

```
docker volume create beta-data
docker-compose build
docker-compose up
```

- After running these commands, make sure all of your Docker containers are running
- View the project in the browser: http://localhost:3000/

***

## Design

CarCar is made up of 3 microservices which interact with one another.

- [Inventory](#inventory-api-descriptions)
- [Sales](#sales-microservice)
- [Services](#service-microservice)

![Project Diagram](CarCar-Diagram.png)

## Inventory API Descriptions


### Manufacturer:

| Action                         | Method | URL                                                 |
| -                              | :-:    | -                                                   |
| List manufacturers             | GET    | `http://localhost:8100/api/manufacturers/`          |
| Manufacturer details           | GET    | `http://localhost:8100/api/manufacturers/<int:id>/` |
| Create manufacturer            | POST   | `http://localhost:8100/api/manufacturers/`          |
| Update manufacturer            | PUT    | `http://localhost:8100/api/manufacturers/<int:id>/` |
| Delete manufacturer            | DELETE | `http://localhost:8100/api/manufacturers/<int:id>/` |

##### Get manufacturers info:

To get manufacturer information, send a GET request to the endpoint. Note that if ```id``` is provided, only that manufactuer's details will be returned. If ```id``` is not specified, all manufacturers info will be returned.

```
URL: http://localhost:8100/api/manufacturers/<int:id>/
Method: GET
```

```json
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Ford"
    },
    {
      "href": "/api/manufacturers/2/",
      "id": 2,
      "name": "Jeep"
    }
  ]
}
```

##### Create a manufacturer:

To create a manufacturer, send a POST request to the API endpoint. Note that ```name``` is the only data that can be specified. ```id``` and ```href``` will be generated automatically.

```
URL: http://localhost:8100/api/manufacturers/
Method: POST
```

```json
{
  "name": "Ford"
}
```

##### Update a manufacturer:

To change the ```name``` of a manufacturer, send a PUT request with the manufacturer ```id``` as shown below.

```
URL: http://localhost:8100/api/manufacturers/<int:id>/
Method: PUT
```

```json
{
  "name": "Chevrolet"
}
```

##### Delete a manufacturer:

To delete a manufacturer, send a DELETE request with the appointment ```id```. Note that if models or automobiles exist that are dependent on a deleted manufacturer, those models and automobiles will also be deleted.

```
URL: http://localhost:8100/api/manufacturers/<int:id>/
Method: DELETE
```

***

### Model:

| Action                          | Method | URL                                          |
| -                               | :-:    | -                                            |
| List vehicle models             | GET    | `http://localhost:8100/api/models/`          |
| Vehicle model details           | GET    | `http://localhost:8100/api/models/<int:id>/` |
| Create vehicle model            | POST   | `http://localhost:8100/api/models/`          |
| Update vehicle model            | PUT    | `http://localhost:8100/api/models/<int:id>/` |
| Delete vehicle model            | DELETE | `http://localhost:8100/api/models/<int:id>/` |


##### Get model info:

To get model information, send a GET request to the endpoint. Note that if ```id``` is provided, only that manufactuer's details will be returned. If ```id``` is not specified, all manufacturers info will be returned.

```
URL: http://localhost:8100/api/models/<int:id>/
Method: GET
```

```json
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "F150 4D",
      "picture_url": "image.newpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Ford"
      }
    },
    {
      "href": "/api/models/2/",
      "id": 2,
      "name": "Wrangler",
      "picture_url": "image.jeeppictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/2/",
        "id": 2,
        "name": "Jeep"
      }
    }
  ]
}

```

##### Create a model:

To create a model, send a POST request to the API endpoint. Note that ```name``` is the only data that can be specified. ```id``` and ```href``` will be generated automatically.

```
URL: http://localhost:8100/api/models/
Method: POST
```

```json
{
  "name": "F150",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer_id": 1
}
```

##### Update a model:

To update a model, send a PUT request with the model ```id``` as shown below.

```
URL: http://localhost:8100/api/models/<int:id>/
Method: PUT
```

```json
{
  "name": "F150 4D",
  "picture_url": "image.newpictureurl.com"
}
```

##### Delete a model:

To delete a model, send a DELETE request with the model ```id```. Note that if automobiles exist that are dependent on a deleted model, those automobiles will also be deleted.

```
URL: http://localhost:8100/api/models/<int:id>/
Method: DELETE
```

***

### Automobile:

- The **'vin'** at the end of the URL endpoints represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action                       | Method | URL                                                   |
| -                            | :-:    | -                                                     |
| List automobiles             | GET    | `http://localhost:8100/api/automobiles/`              |
| Get a specific automobile    | GET    | `http://localhost:8100/api/automobiles/<string:vin>/` |
| Create an automobile         | POST   | `http://localhost:8100/api/automobiles/`              |
| Update a specific automobile | PUT    | `http://localhost:8100/api/automobiles/<string:vin>/` |
| Delete a specific automobile | DELETE | `http://localhost:8100/api/automobiles/<string:vin>/` |

##### Get automobile info:

To get automobile information, send a GET request to the endpoint. Note that if ```vin``` is provided, only that manufactuer's details will be returned. If ```vin``` is not specified, all manufacturers info will be returned.

```
URL: http://localhost:8100/api/automobiles/<int:vin>/
Method: GET
```

```json
{
  "href": "/api/automobiles/MKC1A13YH5AT56T9Q/",
  "id": 1,
  "color": "blue",
  "year": 2020,
  "vin": "MKC1A13YH5AT56T9Q",
  "model": {
    "href": "/api/models/3/",
    "id": 3,
    "name": "RAV4",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/6/",
      "id": 6,
      "name": "Toyota"
    }
  },
  "sold": false
}
```

##### Create a automobile:

To create a automobile, send a POST request to the API endpoint. Note that the data shown in the example below is the only data that can be specified. ```id```, ```href```, and ```sold``` will be generated automatically, and the ```model``` and ```manufacturer``` blocks will be populated from data derived from ```model_id```.

```
URL: http://localhost:8100/api/automobiles/
Method: POST
```

```json
{
  "color": "blue",
  "year": 2020,
  "vin": "MKC1A13YH5AT56T9Q",
  "model_id": 1
}
```

##### Update a automobile:

To update automobile data, send a PUT request with the automobile ```vin``` as shown below. Only the properties shown in the POST example may be used.

```
URL: http://localhost:8100/api/automobiles/<int:vin>/
Method: PUT
```

```json
{
  "color": "red"
}
```

##### Delete a automobile:

To delete a automobile, send a DELETE request with the automobile ```vin```.

```
URL: http://localhost:8100/api/automobiles/<int:vin>/
Method: DELETE
```

***
***

## Sales microservice

The Sales microservice consists of 4 models: AutomobileVO, Customer, Salesperson, and Sale. Sale is the model that interacts with the other three models through the foreign key relationship.

The AutomobileVO is a **value object** that gets data about the Automobile class in the Inventory service using a poller. The sales poller automotically polls the Inventory for automobile data, so the Sales service is constantly getting updated every minute. This integration is important for selling a car to update the automobiles status inside of the Inventory service.


```Salesperson``` model - first_name, last_name, employee_id fields.

```Customer``` model - first_name, last_name, address, and phone_number fields.

```Sale``` model - automobile, salesperson, customer and price fields.

```AutomobileVO``` model - vin, sold fields.

### Sales API Descriptions

### Customer:

| Action                    | Method | URL                                             |
| -                         | :-:    | -                                               |
| List customers            | GET    | `http://localhost:8090/api/customers/`          |
| Create customer           | POST   | `http://localhost:8090/api/customers/`          |
| Customer Details          | GET    | `http://localhost:8090/api/customers/<int:id>/` |
| Delete customer           | DELETE | `http://localhost:8090/api/customers/<int:id>/` |

#### To create a Customer:
```
URL: http://localhost:8090/api/customers/
Method: POST
```

```json
{
  "first_name": "John",
  "last_name": "Doe",
  "phone_number": "867-5309",
  "address": "124 Fake St Orlando, FL 123456"
}
```

#### Return Value of Creating a Customer, the ```id``` will be automatically assigned:

```json
{
  "first_name": "John",
  "last_name": "Doe",
  "phone_number": "867-5309",
  "address": "124 Fake St Orlando, FL 123456",
  "id": 1
}
```
#### List all Customers:

```
URL: http://localhost:8090/api/customers/
Method: GET
```

#### Return value of list all Customers:

```json
{
  "customers": [
    {
      "first_name": "Bob",
      "last_name": "Doe",
      "phone_number": "867-5309",
      "address": "124 Fake St Orlando, FL 123456",
      "id": 1
    },
    {
      "first_name": "Dave",
      "last_name": "Doe",
      "phone_number": "123-4567",
      "address": "125 Other St Orlando, FL 123456",
      "id": 2
    }
  ]
}
```

#### Delete a Customer:
To delete a customer, send a DELETE request to the API endpoint with the customer's ```id```.

```
URL: http://localhost:8090/api/customer/<int:id>/
Method: DELETE
```

### Salesperson:

| Action                 | Method | URL                                                 |
| -                      | :-:    | -                                                   |
| List salespeople       | GET    | `http://localhost:8090/api/salespeople/`            |
| Create salesperson     | POST   | `http://localhost:8090/api/salespeople/`            |
| Salesperson Details    | GET    | `http://localhost:8090/api/salespeople/<int:id>/`   |
| Delete salesperson     | DELETE | `http://localhost:8090/api/salespeople/<int:id>/`   |

#### To create a Salesperson:
```
URL: http://localhost:8090/api/salespeople/
Method: POST
```

```json
{
  "first_name": "Jane",
  "last_name": "Doe",
  "employee_id": "JD123"
}
```

#### Return Value of creating a Salesperson, the ```id``` will be automatically assigned:

```json
{
  "first_name": "Jane",
  "last_name": "Doe",
  "employee_id": "JD321",
  "id": 1
}
```
#### List all salespeople:
```
URL: http://localhost:8090/api/salespeople/
Method: GET
```
#### List all Salespeople Return Value:

```json
{
  "salespeople": [
    {
      "first_name": "Jane",
      "last_name": "Doe",
      "employee_id": "JD321",
      "id": 1
    },
    {
      "first_name": "Bill",
      "last_name": "Doe",
      "employee_id": "BD456",
      "id": 2
    }
  ]
}
```
#### Delete a Salesperson:
To delete a salesperson, send a DELETE request to the API endpoint with the salesperson's ```id```.

```
URL: http://localhost:8090/api/salespeople/<int:id>/
Method: DELETE
```

### Sale:

- the ```id``` value to show a salesperson's salesrecord is the ```id``` **value tied to a salesperson.**

| Action              | Method | URL                                             |
| -                   | :-:    | -                                               |
| List sales          | GET    | `http://localhost:8090/api/sales/`              |
| Create sale         | POST   | `http://localhost:8090/api/sales/`              |
| Sale Details        | GET    | `http://localhost:8090/api/sales/<int:id>`      |
| Delete sale         | DELETE | `http://localhost:8090/api/sales/<int:id>`      |

#### Create a New Sale:
```
URL: http://localhost:8090/api/sales/
Method: POST
```

```json
{
  "automobile": "7BC1A13YH5AT56T6Y",
  "salesperson": "2",
  "customer": "2",
  "price": 19000.00
}
```

#### Return Value of Creating a New Sale, the ```id``` will be automatically assigned:

```json
{
  "salesperson": {
    "first_name": "John",
    "last_name": "Doe",
    "employee_id": "JD123",
    "id": 1
  },
  "customer": {
    "first_name": "Bob",
    "last_name": "Doe",
    "phone_number": "8675309",
    "address": "123 Fake St Orlando, FL 123456",
    "id": 1
  },
  "automobile": {
    "vin": "7BC1A13YH5AT56T6Y",
    "sold": true,
    "id": 34
  },
  "price": 19000.0,
  "id": 1
}
```

#### Salesperson's Sale Details:
```
URL: http://localhost:8090/api/sales/<int:id>/
Method: GET
```
#### Salesperson's Sale Details Return Value:

```json
{
  "salesperson": {
    "first_name": "John",
    "last_name": "Doe",
    "employee_id": "JD123",
    "id": 1
  },
  "customer": {
    "first_name": "Bob",
    "last_name": "Doe",
    "phone_number": "867-5309",
    "address": "123 Fake St Orlando, FL 123456",
    "id": 1
  },
  "automobile": {
    "vin": "7BC1A13YH5AT56T6Y",
    "sold": true,
    "id": 34
  },
  "price": 19000.0,
  "id": 1
}
```
#### List all Sales:

```
URL: http://localhost:8090/api/sales/
Method: GET
```

#### List all Sales Return Value:

```json
{
  "sales": [
    {
      "salesperson": {
        "first_name": "Jane",
        "last_name": "Doe",
        "employee_id": "JD321",
        "id": 1
      },
      "customer": {
        "first_name": "Bob",
        "last_name": "Doe",
        "phone_number": "867-5309",
        "address": "124 Fake St Orlando, FL 123456",
        "id": 1
      },
      "automobile": {
        "vin": "1C3CC5FB2AN120174",
        "sold": true,
        "id": 34
      },
      "price": 15000.0,
      "id": 1
    },
    {
      "salesperson": {
        "first_name": "Bill",
        "last_name": "Doe",
        "employee_id": "BD456",
        "id": 2
      },
      "customer": {
        "first_name": "Dave",
        "last_name": "Doe",
        "phone_number": "111-5309",
        "address": "123 Other St Orlando, FL 123456",
        "id": 2
      },
      "automobile": {
        "vin": "Z23CC5FB2AN120BBE",
        "sold": true,
        "id": 38
      },
      "price": 15000.0,
      "id": 2
    },
  ]
}
```
#### Delete a Sale:
To delete a sale, send a DELETE request to the API endpoint with the sale ```id```.

```
URL: http://localhost:8090/api/sales/<int:id>/
Method: DELETE
```

***
***

## Service microservice

Service microservice allows for the creation and tracking of service technicians and service appointments.

Service appointments may be created for any vehicle, but vehicles purchased from CarCar will be given VIP status.  VIP customers are guaranteed[^1] oil changes for life, among other benefits.

[^1]: Subject to change.

Service microservice consists of two parts:

- [Technician staff](#technicians)
- [Service appointments](#service-appointments)

***

### API Descriptions

#### Technicians:

| Action                                | Method  | URL                                               |
| -                                     | :-:     | -                                                 |
| List technicians                      | GET     | `http://localhost:8080/api/technicians/`          |
| Create a technician                   | POST    | `http://localhost:8080/api/technicians/`          |
| Delete a specific technician          | DELETE  | `http://localhost:8080/api/technicians/<int:id>/` |

##### List technicians:

To get information for all service technicians, send a GET request to the endpoint.

```
URL: http://localhost:8080/api/technicians/
Method: GET
```

```json
{
  "technicians": [
    {
      "id": 1,
      "first_name": "John",
      "last_name": "Smith",
      "employee_id": "0123456789"
    }
  ]
}
```

##### Create technician:

To create a model, send a POST request to the API endpoint. Note that the ```id``` property should not be provided, as it will be generated automatically.

```
URL: http://localhost:8080/api/technicians/
Method: POST
```

```json
{
  "first_name": "John",
  "last_name": "Smith",
  "employee_id": "0123456789"
}
```

##### Delete a technician:

To delete a technician, send a DELETE request to the API endpoint with the technician ```id```.

```
URL: http://localhost:8080/api/technicians/<int:id>/
Method: DELETE
```

#### Service Appointments:

| Action                                | Method  | URL                                                       |
| -                                     | :-:     | -                                                         |
| List appointments                     | GET     | `http://localhost:8080/api/appointments/`                 |
| Appointment history                   | GET     | `http://localhost:8080/api/appointments/history/`         |
| Create an appointment                 | POST    | `http://localhost:8080/api/appointments/`                 |
| Set appointment status to "canceled"  | PUT     | `http://localhost:8080/api/appointments/<int:id>/cancel/` |
| Set appointment status to "finished"  | PUT     | `http://localhost:8080/api/appointments/<int:id>/finish/` |
| Delete an appointment                 | DELETE  | `http://localhost:8080/api/appointments/<int:id>/`        |

##### List appointments:

This will return a list of all active service appointments.  For a service appointment to be active the ```status``` key must be set to ```active```.  Other possible ```status``` values are: ```canceled``` and ```finished```.  The following is an example response to an api list call returning one active appointment:

```
URL: http://localhost:8080/api/appointments/
Method: GET
```

```json
{
  "appointments": [
    {
      "id": 1,
      "date_time": "2023-12-27T16:30:00+00:00",
      "reason": "Exhaust",
      "status": "active",
      "vin": "J8371Yh7381J716",
      "is_vip": true,
      "customer": "Bill Williams",
      "technician": {
        "id": 2,
        "first_name": "Steven",
        "last_name": "Wright",
        "employee_id": "9876543210"
      }
    }
  ]
}
```

##### Appointment history:

Nearly identical to "List appointments", the ```http://localhost:8080/api/appointments/history``` endpoint will return all service appointments regardless of their status.

```
URL: http://localhost:8080/api/appointments/
Method: GET
```

```json
{
  "appointments": [
    {
      "id": 1,
      "date_time": "2023-12-27T16:30:00+00:00",
      "reason": "Exhaust",
      "status": "active",
      "vin": "J8371Yh7381J716",
      "is_vip": true,
      "customer": "Bill Williams",
      "technician": {
        "id": 2,
        "first_name": "Steven",
        "last_name": "Wright",
        "employee_id": "9876543210"
      }
    },
    {
      "id": 2,
      "date_time": "2024-02-22T16:05:00+00:00",
      "reason": "Tires",
      "status": "active",
      "vin": "1C3CC5FB2AN120174",
      "is_vip": true,
      "customer": "James Woods",
      "technician": {
        "id": 1,
        "first_name": "John",
        "last_name": "Smith",
        "employee_id": "0123456789"
      }
    }
  ]
}
```

##### Create an appointment:

To create a service appointment, submit a POST request with the data in standard JSON format as in the example below.  The ```technician``` property should be a valid technician ```id```.  Note: The ```status``` and ```is_vip``` fields are optional and will be set by default to ```"active"``` and ```true``` respectively.

```
URL: http://localhost:8080/api/appointments/
Method: POST
```

```json
{
  "date_time": "2023-12-27T16:30:00+00:00",
  "reason": "Exhaust",
  "status": "active",
  "vin": "J8371Yh7381J716",
  "is_vip": true,
  "customer": "Bill Williams",
  "technician": 1
}
```

##### Set appointment status to "canceled":

To change the status of an appointment to canceled, send a PUT request to the ```/cancel/``` API endpoint with the appointment id.

```
URL: http://localhost:8080/api/appointments/<int:id>/cancel/
Method: PUT
```

##### Set appointment status to "finished":

To change the status of an appointment to finished, send a PUT request to the ```/finish/``` API endpoint with the appointment id.

```
URL: http://localhost:8080/api/appointments/<int:id>/finish/
Method: PUT
```

##### Delete an appointment:

To delete a service appointment, send a DELETE request with the appointment ```id```.

```
URL: http://localhost:8080/api/appointments/<int:id>/
Method: DELETE
```
