from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    TechnicianEncoder,
    AppointmentEncoder,
)
from .models import AutomobileVO, Technician, Appointment


def set_is_vip(appointments):
    for appointment in appointments:
        if AutomobileVO.objects.filter(vin=appointment.vin):
            appointment.is_vip = True
        else:
            appointment.is_vip = False
        appointment.save()
    return None


@require_http_methods(["GET", "POST", "DELETE"])
def api_technicians(request, technician_id=None):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response
    else:  # DELETE
        try:
            technician = Technician.objects.get(id=technician_id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST", "DELETE", "PUT"])
def api_appointments(request, appointment_id=None):
    if request.method == "GET":
        # Active Appointments List
        try:
            appointments = Appointment.objects.filter(status="active")
            set_is_vip(appointments)

            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = content["technician"]
            technician = Technician.objects.get(id=technician)
            content["technician"] = technician

            appointment = Appointment.objects.create(**content)
            set_is_vip([appointment])

            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the appointment"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=appointment_id)

            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response
    else:  # PUT
        try:
            appointment = Appointment.objects.get(id=appointment_id)

            url_endpoint = request.path[-7:-1]
            new_status = url_endpoint + 'ed'
            appointment.status = new_status
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_appointments_history(request):
    try:
        appointments = Appointment.objects.all()
        set_is_vip(appointments)

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
