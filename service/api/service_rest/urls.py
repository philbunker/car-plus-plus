from django.urls import path
from .views import api_technicians, api_appointments, api_appointments_history

urlpatterns = [
    path("technicians/", api_technicians, name="api_list_technicians"),
    path("technicians/<int:technician_id>/", api_technicians, name="api_delete_technician"),

    path("appointments/history/", api_appointments_history, name="api_appointments_history"),

    path("appointments/<int:appointment_id>/", api_appointments, name="api_delete_appointment"),
    path("appointments/<int:appointment_id>/cancel/", api_appointments, name="api_cancel_appointment"),
    path("appointments/<int:appointment_id>/finish/", api_appointments, name="api_finish_appointment"),

    path("appointments/", api_appointments, name="api_list_appointments"),
]
