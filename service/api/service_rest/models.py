from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, default='Unknown VIN')
    sold = models.BooleanField()


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=1000)
    status = models.CharField(max_length=20, default="active")
    vin = models.CharField(max_length=17)
    is_vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.CASCADE,
    )
